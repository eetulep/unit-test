const expect = require('chai').expect;
const { assert } = require('chai');
const mylib = require('../src/mylib');

test = ""

describe("Our first unit test", () => {
  before (() => {
    test = "hello"
  }) 

  it("Can add 1 and 2 together", () => {
    // Tests
    expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
  });

  it("Can subtract 5 with 3", () => {
    expect(mylib.subtract(5,3)).equal(2, "5 - 3 is not 2 for some reason?")
  });

  it("returns an exception when the divisor is 0", () => {
    const divide = () => mylib.divide(8, 0);
    assert.throws(divide, Error);
  });

  it("multiply 5 with 8", () => {
    expect(mylib.multiply(5,8)).equal(40, "5 * 8 is not 40 for some reason?")
  })

  after(() => {
    console.log(test)
    test = ""
    console.log("testing completed!")
  })
});