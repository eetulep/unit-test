# Dokumentointi

index.html = tyhjä

main.js = Console.loggaa mylibs tiedoston funktioita

mylib = matemaattisia laskuja

example.test =
  before: antaa test arvon hello
  it: testaa kaikki mylib.js filen funktiot
  after: console.loggaa test ja muuttaa test arvon tyhjäksi