// Basic arithmetic operations
const mylib = {

  // Multiline arrow funcktion
  add: (a, b) => {
    const sum = a + b;
    return sum;
  },

  subtract: (a, b) => {
    return a - b
  },

  divide(dividend, divisor) {
    if (divisor === 0) {
      throw new Error('the quotient of a number and 0 is undefined');
    } else {
      return dividend / divisor;
    }
  },

  multiply: function(a, b) {
    return a * b;
  }
};

module.exports = mylib;